import React from 'react';
import shortid from 'shortid';
import Card from './Card.jsx';

const CardList = ({ robots }) => {
    const simpleCard = robots.map(robot => (
        <Card email={robot.email} id={robot.id} name={robot.name} key={shortid.generate()} />
    ));
    return (
        <div className="card_wrapper">
            {simpleCard}
        </div>
    );
};

export default CardList;