import React from "react";

const Searchbox = ({ searchString, searchChange }) => {
  return (
    <div className="ma2">
      <input type="search" placeholder="search for robots" onChange={searchChange} />
    </div>
  );
};

export default Searchbox;
