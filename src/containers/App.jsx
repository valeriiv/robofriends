import React, { Component, Fragment } from "react";
import CardList from "../components/CardList.jsx";
import Searchbox from "../components/Searchbox.jsx";
import Scroll from "../components/Scroll";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      robots: [],
      searchString: ""
    };
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  async componentDidMount() {
    const link = await fetch(`https://jsonplaceholder.typicode.com/users`);
    const users = await link.json();
    this.setState({ robots: users });
  }

  onSearchChange(event) {
    this.setState({ searchString: event.target.value });
  }
  // onSearchChange = (event) => {
  //     const filteredRobots = this.state.robots.filter(robot => robot.name.toLowerCase().includes(this.state.searchString.toLowerCase()));
  //     console.log(filteredRobots);
  // }

  render() {
    const { robots, searchString } = this.state;
    const filteredRobots = robots.filter(robot =>
      robot.name.toLowerCase().includes(searchString.toLowerCase())
    );
      return !robots.length ? <h1>Loading...</h1> :
     (
      <Fragment>
        <h1>Robofriends</h1>
        <Searchbox searchChange={this.onSearchChange} />
        <Scroll>
          <CardList robots={filteredRobots} />
        </Scroll>
      </Fragment>
    );
  }
}
